package structure_dumper

import (
	"reflect"
	"testing"
	"time"
)

// Проверка доступности процессора
func Test_timeConverterProcessor_IsAvailable(t *testing.T) {
	type args struct {
		object interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование на доступном значении",
			args: args{
				object: time.Now(),
			},
			want: true,
		},
		{
			name: "Тестирование на не доступном значении",
			args: args{
				object: "11",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := timeConverterProcessor{}
			if got := s.IsAvailable(tt.args.object); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации значения
func Test_timeConverterProcessor_Process(t *testing.T) {
	val, _ := time.Parse(time.RFC3339, `2020-06-04T15:40:38.236791+03:00`)
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name string
		args args
		want processResult
	}{
		{
			name: "Тестирование конвертации",
			args: args{
				object:   val,
				pointers: []uintptr{},
			},
			want: processResult{
				result:   "time(2020-06-04T15:40:38+03:00)",
				pointers: []uintptr{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := timeConverterProcessor{}
			if got := s.Process(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Process() = %v, want %v", got, tt.want)
			}
		})
	}
}
