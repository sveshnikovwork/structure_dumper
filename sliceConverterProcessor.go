package structure_dumper

import "reflect"

// Конвертер срезов
type sliceConverterProcessor struct {
	converter structureConverterInterface
}

// Сеттер основного конвертера
func (s *sliceConverterProcessor) SetConverter(c structureConverterInterface) {
	s.converter = c
}

// Проверяет доступность конвертера
func (s sliceConverterProcessor) IsAvailable(object interface{}) bool {
	objType := reflect.TypeOf(object).Kind()

	return objType == reflect.Slice || objType == reflect.Array
}

// Обработка значения конвертером
func (s sliceConverterProcessor) Process(object interface{}, pointers []uintptr) processResult {
	if !s.IsAvailable(object) {
		return processResult{[]interface{}{}, pointers}
	}

	var result []interface{}
	valueOfSlice := reflect.ValueOf(object)
	for i := 0; i < valueOfSlice.Len(); i++ {
		convertResult := s.converter.Convert(valueOfSlice.Index(i).Interface(), pointers)

		result = append(result, convertResult.result)
		pointers = convertResult.pointers
	}

	return processResult{result, pointers}
}

// Фабрика процессора
func newSliceConverterProcessor() converterProcessorInterface {
	return &sliceConverterProcessor{}
}
