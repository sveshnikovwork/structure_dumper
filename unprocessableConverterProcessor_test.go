package structure_dumper

import (
	"reflect"
	"testing"
)

// Тестирование метода доступности процессора
func Test_unprocessableConverterProcessor_IsAvailable(t *testing.T) {
	type fields struct {
		types []reflect.Kind
	}
	type args struct {
		object interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "Тестирование на валидном значении",
			fields: fields{unprocessableTypes},
			args: args{
				func() {},
			},
			want: true,
		},
		{
			name:   "Тестирование на не валидном значении",
			fields: fields{unprocessableTypes},
			args: args{
				struct{}{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := unprocessableConverterProcessor{
				types: tt.fields.types,
			}
			if got := u.IsAvailable(tt.args.object); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование метода обработки процессора
func Test_unprocessableConverterProcessor_Process(t *testing.T) {
	type fields struct {
		types []reflect.Kind
	}
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   processResult
	}{
		{
			name:   "Тестирование передачи функции с не пустым массивом указателей",
			fields: fields{unprocessableTypes},
			args:   args{func() {}, []uintptr{1}},
			want:   processResult{"func", []uintptr{1}},
		},
		{
			name:   "Тестирование передачи канала с не пустым массивом указателей",
			fields: fields{unprocessableTypes},
			args:   args{make(chan bool), []uintptr{1}},
			want:   processResult{"chan", []uintptr{1}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := unprocessableConverterProcessor{
				types: tt.fields.types,
			}
			if got := u.Process(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Process() = %v, want %v", got, tt.want)
			}
		})
	}
}
