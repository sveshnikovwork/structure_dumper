package structure_dumper

// Фасад конвертера
type structureConverterFacade struct {
	systemConverter structureConverterInterface
}

// Конвертирует переданный объект данных в простую коллекцию значений или простое значение
func (s structureConverterFacade) Convert(object interface{}) interface{} {
	result := s.systemConverter.Convert(object, []uintptr{})

	return result.result
}

// Фабрика фасада конвертера
func NewStructureConverter() StructureConverterInterface {
	return &structureConverterFacade{
		systemConverter: structureConverterSingleton,
	}
}
