package structure_dumper

import (
	"reflect"
	"testing"
)

// Тестирование метода доступности конвертера
func Test_pointerConverterProcessor_IsAvailable(t *testing.T) {
	str := "test"

	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "Тестирование передачи строки",
			fields: fields{},
			args: args{
				"1",
			},
			want: false,
		},
		{
			name:   "Тестирование передачи указателя",
			fields: fields{},
			args: args{
				&str,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := pointerConverterProcessor{
				converter: tt.fields.converter,
			}
			if got := p.IsAvailable(tt.args.object); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование метода конвертации
func Test_pointerConverterProcessor_Process(t *testing.T) {
	str := "test string"

	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   processResult
	}{
		{
			name: "Тестирование передачи строки",
			fields: fields{
				converter: &converterMock{
					IsCalled: false,
				},
			},
			args: args{"1", []uintptr{}},
			want: processResult{nil, []uintptr{}},
		},
		{
			name: "Тестирование передачи указателя",
			fields: fields{
				converter: &converterMock{
					IsCalled: false,
				},
			},
			args: args{&str, []uintptr{}},
			want: processResult{map[string]interface{}{
				"pointerTo": reflect.ValueOf(&str).Pointer(),
				"value":     str,
			}, []uintptr{reflect.ValueOf(&str).Pointer()}},
		},
		{
			name: "Тестирование передачи указателя c не пустым массивом указателей.",
			fields: fields{
				converter: &converterMock{
					IsCalled: false,
				},
			},
			args: args{&str, []uintptr{1}},
			want: processResult{map[string]interface{}{
				"pointerTo": reflect.ValueOf(&str).Pointer(),
				"value":     str,
			}, []uintptr{1, reflect.ValueOf(&str).Pointer()}},
		},
		{
			name: "Тестирование передачи указателя c не пустым массивом указателей, включающим обработанный.",
			fields: fields{
				converter: &converterMock{
					IsCalled: false,
				},
			},
			args: args{&str, []uintptr{reflect.ValueOf(&str).Pointer()}},
			want: processResult{map[string]interface{}{
				"pointerTo": reflect.ValueOf(&str).Pointer(),
				"value":     nil,
			}, []uintptr{reflect.ValueOf(&str).Pointer()}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := pointerConverterProcessor{
				converter: tt.fields.converter,
			}
			if got := p.Process(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Process() = %v, want %v", got, tt.want)
			}
		})
	}
}
