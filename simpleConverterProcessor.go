package structure_dumper

import (
	"fmt"
	"reflect"
)

// Конвертер строкового значения
type simpleConverterProcessor struct {
	converter   structureConverterInterface
	simpleTypes []reflect.Kind
}

// Сеттер основного конвертера
func (s *simpleConverterProcessor) SetConverter(c structureConverterInterface) {
	s.converter = c
}

// Проверяет доступность конвертера
func (s simpleConverterProcessor) IsAvailable(object interface{}) bool {
	if nil == object {
		return true
	}

	objectType := reflect.TypeOf(object).Kind()
	for _, availableType := range s.simpleTypes {
		if objectType == availableType {
			return true
		}
	}

	return false
}

// Обработка значения конвертером
func (s simpleConverterProcessor) Process(object interface{}, pointers []uintptr) processResult {
	if nil == object {
		return processResult{nil, pointers}
	}

	objectType := reflect.TypeOf(object)

	return processResult{fmt.Sprintf("%v(%v)", objectType.Name(), object), pointers}
}

// Фабрика процессора
func newSimpleConverter(simpleTypes []reflect.Kind) converterProcessorInterface {
	return &simpleConverterProcessor{
		converter:   nil,
		simpleTypes: simpleTypes,
	}
}
