package structure_dumper

import "reflect"

// Конвертер указателей
type pointerConverterProcessor struct {
	converter structureConverterInterface
}

// Сеттер основного конвертера
func (p *pointerConverterProcessor) SetConverter(c structureConverterInterface) {
	p.converter = c
}

// Проверяет доступность конвертера
func (p pointerConverterProcessor) IsAvailable(object interface{}) bool {
	if nil == object {
		return false
	}

	validTypes := []reflect.Kind{
		reflect.Ptr,
		reflect.Uintptr,
	}

	objectType := reflect.TypeOf(object).Kind()
	for _, validType := range validTypes {
		if validType == objectType {
			return true
		}
	}

	return false
}

// Обработка значения конвертером
func (p pointerConverterProcessor) Process(object interface{}, pointers []uintptr) processResult {
	if !p.IsAvailable(object) {
		return processResult{nil, pointers}
	}

	value := reflect.ValueOf(object)
	value.Convert(value.Type())

	if value.IsZero() || value.Kind() == reflect.Uintptr {
		return processResult{nil, pointers}
	}

	converted := value.Elem().Interface()

	pointer := value.Pointer()
	result := map[string]interface{}{
		"pointerTo": pointer,
		"value":     nil,
	}

	if true == p.pointerProcessed(pointer, pointers) {
		return processResult{result, pointers}
	}

	pointers = append(pointers, pointer)
	convertResult := p.converter.Convert(converted, pointers)

	result["value"] = convertResult.result
	pointers = convertResult.pointers

	return processResult{result, pointers}
}

// Проверяет, обработан ли уже текущий указатель.
func (p pointerConverterProcessor) pointerProcessed(pointer uintptr, pointers []uintptr) bool {
	for _, processedPointer := range pointers {
		if processedPointer == pointer {
			return true
		}
	}

	return false
}

// Фабрика процессора
func newPointerConverterProcessor() converterProcessorInterface {
	return &pointerConverterProcessor{}
}
