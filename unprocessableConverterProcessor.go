package structure_dumper

import "reflect"

type unprocessableConverterProcessor struct {
	types []reflect.Kind
}

// Сеттер основного конвертера
func (u unprocessableConverterProcessor) SetConverter(structureConverterInterface) {}

// Проверяет доступность конвертера
func (u unprocessableConverterProcessor) IsAvailable(object interface{}) bool {
	objectType := reflect.TypeOf(object).Kind()
	for _, availableType := range u.types {
		if objectType == availableType {
			return true
		}
	}

	return false
}

// Обработка значения конвертером
func (u unprocessableConverterProcessor) Process(object interface{}, pointers []uintptr) processResult {
	return processResult{reflect.TypeOf(object).Kind().String(), pointers}
}

// Фабрика процессора
func newUnprocessableConverterProcessor(unprocessableTypes []reflect.Kind) converterProcessorInterface {
	return &unprocessableConverterProcessor{
		types: unprocessableTypes,
	}
}
