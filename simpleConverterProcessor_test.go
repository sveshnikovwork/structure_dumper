package structure_dumper

import (
	"reflect"
	"testing"
)

// Тестирование метода доступности конвертера
func Test_stringConverterProcessor_IsAvailable(t *testing.T) {
	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "Тестирование на валидном значении",
			fields: fields{},
			args: args{
				"test",
			},
			want: true,
		},
		{
			name:   "Тестирование на не валидном значении",
			fields: fields{},
			args: args{
				struct{}{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleConverterProcessor{
				converter:   tt.fields.converter,
				simpleTypes: simpleTypes,
			}
			if got := s.IsAvailable(tt.args.object); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование метода обработки значения
func Test_simpleConverterProcessor_Process(t *testing.T) {
	type fields struct {
		converter   structureConverterInterface
		simpleTypes []reflect.Kind
	}
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   processResult
	}{
		{
			name:   "Тестирование передачи строки",
			fields: fields{},
			args:   args{"1", []uintptr{}},
			want:   processResult{"string(1)", []uintptr{}},
		},
		{
			name:   "Тестирование передачи не пустого набора указателей",
			fields: fields{},
			args:   args{1, []uintptr{1}},
			want:   processResult{"int(1)", []uintptr{1}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleConverterProcessor{
				converter:   tt.fields.converter,
				simpleTypes: tt.fields.simpleTypes,
			}
			if got := s.Process(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Process() = %v, want %v", got, tt.want)
			}
		})
	}
}
