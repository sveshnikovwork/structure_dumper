package structure_dumper

import (
	"reflect"
	"testing"
)

// Тестирование метода доступности
func Test_mapConverterProcessor_IsAvailable(t *testing.T) {
	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "Тестирование передачи функции",
			fields: fields{&converterMock{}},
			args: args{
				func() {},
			},
			want: false,
		},
		{
			name:   "Тестирование передачи хэш-таблицы",
			fields: fields{&converterMock{}},
			args: args{
				map[string]interface{}{},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mapConverterProcessor{
				converter: tt.fields.converter,
			}
			if got := m.IsAvailable(tt.args.object); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование метода обработки процессора
func Test_mapConverterProcessor_Process(t *testing.T) {
	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   processResult
	}{
		{
			name:   "Тестирование передачи хэш-таблицы",
			fields: fields{&converterMock{}},
			args: args{
				map[string]interface{}{"test": "test1"},
				[]uintptr{},
			},
			want: processResult{map[string]interface{}{"test": "test1"}, []uintptr{}},
		},
		{
			name:   "Тестирование передачи хэш-таблицы c числом",
			fields: fields{&converterMock{}},
			args: args{
				map[string]interface{}{"test": "test1", "test2": 1},
				[]uintptr{},
			},
			want: processResult{map[string]interface{}{"test": "test1", "test2": 1}, []uintptr{}},
		},
		{
			name:   "Тестирование передачи пустой хэш-таблицы",
			fields: fields{&converterMock{}},
			args: args{
				map[string]interface{}{},
				[]uintptr{},
			},
			want: processResult{map[string]interface{}{}, []uintptr{}},
		},
		{
			name:   "Тестирование передачи не пустого массива указателей",
			fields: fields{&converterMock{}},
			args: args{
				map[string]interface{}{},
				[]uintptr{1},
			},
			want: processResult{map[string]interface{}{}, []uintptr{1}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mapConverterProcessor{
				converter: tt.fields.converter,
			}
			if got := m.Process(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Process() = %v, want %v", got, tt.want)
			}
		})
	}
}
