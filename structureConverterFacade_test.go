package structure_dumper

import (
	"testing"
)

// Тестирование фасада конвертера. Тестируется вызов основного конвертера.
func Test_structureConverterFacade_Convert(t *testing.T) {
	type fields struct {
		systemConverter structureConverterInterface
	}
	type args struct {
		object interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "Тестирование вызова основного конвертера из фасада",
			fields: fields{&converterMock{IsCalled: false}},
			args:   args{"test"},
			want:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := structureConverterFacade{
				systemConverter: tt.fields.systemConverter,
			}

			s.Convert(tt.args.object)
			converter, _ := tt.fields.systemConverter.(*converterMock)

			if converter.IsCalled != tt.want {
				t.Errorf("Convert() system converter call status: %v, want %v", converter.IsCalled, tt.want)
			}
		})
	}
}
