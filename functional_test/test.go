package main

import (
	"encoding/json"

	"structure_dumper"
)

func main() {
	converter := structure_dumper.NewStructureConverter()
	result := converter.Convert(converter)

	jsonVal, _ := json.MarshalIndent(result, "", "    ")
	jsonString := string(jsonVal)

	println(jsonString)
}
