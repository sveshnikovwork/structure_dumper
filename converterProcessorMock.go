package structure_dumper

// Подставка для тестирования конвертера
type converterProcessorMock struct {
	isAvailable bool
	IsCalled    bool
}

// Сеттер основного конвертера
func (c converterProcessorMock) SetConverter(structureConverterInterface) {}

// Проверяет доступность конвертера
func (c converterProcessorMock) IsAvailable(object interface{}) bool {
	return c.isAvailable
}

// Обработка значения конвертером
func (c *converterProcessorMock) Process(object interface{}, pointers []uintptr) processResult {
	c.IsCalled = true

	return processResult{object, pointers}
}
