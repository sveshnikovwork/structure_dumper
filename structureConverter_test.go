package structure_dumper

import (
	"reflect"
	"testing"
)

// Тестирование корректности вызова процессоров внутри конвертера
func Test_structureConverter_Convert_ProcessorPriority(t *testing.T) {
	type fields struct {
		processors []converterProcessorInterface
	}
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []bool
	}{
		{
			name: "Тестирование с передачей 2 процессоров. Первый должен быть вызван.",
			fields: fields{[]converterProcessorInterface{
				&converterProcessorMock{isAvailable: true},
				&converterProcessorMock{isAvailable: false},
			}},
			args: args{"test", []uintptr{}},
			want: []bool{
				true,
				false,
			},
		},
		{
			name: "Тестирование с передачей 2 процессоров. Второй должен быть вызван.",
			fields: fields{[]converterProcessorInterface{
				&converterProcessorMock{isAvailable: false},
				&converterProcessorMock{isAvailable: true},
			}},
			args: args{"test", []uintptr{}},
			want: []bool{
				false,
				true,
			},
		},
		{
			name: "Тестирование с передачей 2 процессоров. Ни один не должен быть вызван.",
			fields: fields{[]converterProcessorInterface{
				&converterProcessorMock{isAvailable: false},
				&converterProcessorMock{isAvailable: false},
			}},
			args: args{"test", []uintptr{}},
			want: []bool{
				false,
				false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := structureConverter{
				processors: tt.fields.processors,
			}

			s.Convert(tt.args.object, tt.args.pointers)

			for i, processor := range tt.fields.processors {
				processor, _ := processor.(*converterProcessorMock)
				if processor.IsCalled != tt.want[i] {
					t.Errorf("Convert() %v processor call status: %v, want %v", i, processor.IsCalled, tt.want[i])
				}
			}
		})
	}
}

// Тестирование результата работы конвертера
func Test_structureConverter_Convert_ProcessorResult(t *testing.T) {
	type fields struct {
		processors []converterProcessorInterface
	}
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   processResult
	}{
		{
			name: "Тестирование с передачей выражения и не пустого значения указателей. Есть подходящий процессор.",
			fields: fields{[]converterProcessorInterface{
				&converterProcessorMock{isAvailable: true},
			}},
			args: args{"test", []uintptr{1}},
			want: processResult{"test", []uintptr{1}},
		},
		{
			name: "Тестирование с передачей выражения и пустого значения указателей. Есть подходящий процессор.",
			fields: fields{[]converterProcessorInterface{
				&converterProcessorMock{isAvailable: true},
			}},
			args: args{"test", []uintptr{}},
			want: processResult{"test", []uintptr{}},
		},
		{
			name: "Тестирование с передачей выражения и не пустого значения указателей. Нет подходящего процессора.",
			fields: fields{[]converterProcessorInterface{
				&converterProcessorMock{isAvailable: false},
			}},
			args: args{"test", []uintptr{1}},
			want: processResult{nil, []uintptr{1}},
		},
		{
			name: "Тестирование с передачей выражения и пустого значения указателей. Нет подходящего процессора.",
			fields: fields{[]converterProcessorInterface{
				&converterProcessorMock{isAvailable: false},
			}},
			args: args{"test", []uintptr{}},
			want: processResult{nil, []uintptr{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := structureConverter{
				processors: tt.fields.processors,
			}
			if got := s.Convert(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() = %v, want %v", got, tt.want)
			}
		})
	}
}
