package structure_dumper

import (
	"fmt"
	"reflect"
)

// Конвертер хэш-таблиц
type mapConverterProcessor struct {
	converter structureConverterInterface
}

// Сеттер основного конвертера
func (m *mapConverterProcessor) SetConverter(c structureConverterInterface) {
	m.converter = c
}

// Проверяет доступность конвертера
func (m mapConverterProcessor) IsAvailable(object interface{}) bool {
	return reflect.TypeOf(object).Kind() == reflect.Map
}

// Обработка значения конвертером
func (m mapConverterProcessor) Process(object interface{}, pointers []uintptr) processResult {
	result := map[string]interface{}{}
	value := reflect.ValueOf(object)

	for _, mapKey := range value.MapKeys() {
		value := value.MapIndex(mapKey).Interface()
		convertResult := m.converter.Convert(value, pointers)

		result[fmt.Sprintf("%s", mapKey.Interface())] = convertResult.result
		pointers = convertResult.pointers
	}

	return processResult{result, pointers}
}

// Фабрика процессора
func newMapConverterProcessor() converterProcessorInterface {
	return &mapConverterProcessor{}
}
