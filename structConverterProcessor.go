package structure_dumper

import (
	"reflect"
	"unsafe"
)

// Конвертер структур
type structConverterProcessor struct {
	converter structureConverterInterface
}

// Сеттер основного конвертера
func (s *structConverterProcessor) SetConverter(c structureConverterInterface) {
	s.converter = c
}

// Проверяет доступность конвертера
func (s structConverterProcessor) IsAvailable(object interface{}) bool {
	return reflect.TypeOf(object).Kind() == reflect.Struct
}

// Обработка значения конвертером
func (s structConverterProcessor) Process(object interface{}, pointers []uintptr) processResult {
	// Клонируем структуру, на случай, если она не адресуемая
	objectVal := reflect.ValueOf(object)
	objectClone := reflect.New(objectVal.Type()).Elem()
	objectClone.Set(objectVal)

	// Вычисляем тип переданной структуры
	objectType := reflect.TypeOf(object)

	structName := `anonymous`
	if len(objectType.Name()) > 0 {
		structName = objectType.Name()
	}

	packageName := `-`
	if len(objectType.PkgPath()) > 0 {
		packageName = objectType.PkgPath()
	}

	result := map[string]interface{}{
		"struct":  structName,
		"package": packageName,
		"fields":  nil,
	}

	fields := map[string]interface{}{}
	for i := 0; i < objectType.NumField(); i++ {
		field := objectClone.Field(i)

		// Через указатель создаем новую переменную, которая по сути указывает на тоже адресное пространство в
		// памяти, что и поле в структуре (Включая не экспортированные поля)
		clearElem := reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).Elem().Interface()

		// Запускаем конвертацию полученного значения поля
		convertResult := s.converter.Convert(clearElem, pointers)

		fields[objectType.Field(i).Name] = convertResult.result
		pointers = convertResult.pointers
	}

	result[`fields`] = fields

	return processResult{result, pointers}
}

// Фабрика процессора
func newStructConverterProcessor() converterProcessorInterface {
	return &structConverterProcessor{}
}
