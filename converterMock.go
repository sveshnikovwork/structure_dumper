package structure_dumper

// Подставка для тестирования. Реализация конвертера
type converterMock struct {
	IsCalled bool // Статус вызова конвертера
}

// Конвертирует переданный объект данных в простую коллекцию значений или простое значение
func (c *converterMock) Convert(object interface{}, pointers []uintptr) processResult {
	c.IsCalled = true

	return processResult{object, pointers}
}
