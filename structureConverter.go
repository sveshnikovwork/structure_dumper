package structure_dumper

// Конвертер объектов в простые хэш-таблицы, пригодные для JSON сериализации
type structureConverter struct {
	processors []converterProcessorInterface
}

// Конвертирует переданный объект данных в простую коллекцию значений или простое значение
func (s structureConverter) Convert(object interface{}, pointers []uintptr) processResult {
	for _, processor := range s.processors {
		if true == processor.IsAvailable(object) {
			return processor.Process(object, pointers)
		}
	}

	return processResult{nil, pointers}
}

// Фабрика системного конвертера
func newStructureConverter() structureConverterInterface {
	processors := []converterProcessorInterface{
		newPointerConverterProcessor(),
		newSimpleConverter(simpleTypes),
		newComplexConverterProcessor(),
		newSliceConverterProcessor(),
		newMapConverterProcessor(),
		newUnprocessableConverterProcessor(unprocessableTypes),
		newTimeConverterProcessor(),
		newStructConverterProcessor(),
	}

	converter := &structureConverter{processors: processors}
	for _, processor := range processors {
		processor.SetConverter(converter)
	}

	return converter
}
