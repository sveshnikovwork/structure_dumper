package structure_dumper

import (
	"reflect"
	"testing"
)

// Тестирование метода доступности процессора
func Test_sliceConverterProcessor_IsAvailable(t *testing.T) {
	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "Тестирование передачи строки",
			fields: fields{},
			args: args{
				"1",
			},
			want: false,
		},
		{
			name:   "Тестирование передачи среза",
			fields: fields{},
			args: args{
				[]string{},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := sliceConverterProcessor{
				converter: tt.fields.converter,
			}
			if got := s.IsAvailable(tt.args.object); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование метода обработки данных
func Test_sliceConverterProcessor_Process(t *testing.T) {
	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   processResult
	}{
		{
			name:   "Тестирование передачи строки с не пустым массивом указателей",
			fields: fields{&converterMock{}},
			args:   args{"1", []uintptr{1}},
			want:   processResult{[]interface{}{}, []uintptr{1}},
		},
		{
			name:   "Тестирование передачи среза с не пустым массивом указателей",
			fields: fields{&converterMock{}},
			args:   args{[]string{"test"}, []uintptr{1}},
			want:   processResult{[]interface{}{"test"}, []uintptr{1}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := sliceConverterProcessor{
				converter: tt.fields.converter,
			}
			if got := s.Process(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Process() = %v, want %v", got, tt.want)
			}
		})
	}
}
