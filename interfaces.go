package structure_dumper

// Результат работы процессора/конвертера
type processResult struct {
	result   interface{}
	pointers []uintptr
}

// Процессор конвертации
type converterProcessorInterface interface {
	// Сеттер основного конвертера
	SetConverter(structureConverterInterface)

	// Проверяет доступность конвертера
	IsAvailable(object interface{}) bool

	// Обработка значения конвертером
	Process(object interface{}, pointers []uintptr) processResult
}

// Конвертер, не обрабатывающий цикличные ссылки
type structureConverterInterface interface {
	// Конвертирует переданный объект данных в простую коллекцию значений или простое значение
	Convert(object interface{}, pointers []uintptr) processResult
}
