package structure_dumper

import (
	"fmt"
	"reflect"
)

// Процессор комплексных чисел
type complexConverterProcessor struct{}

// Сеттер основного конвертера
func (c complexConverterProcessor) SetConverter(structureConverterInterface) {}

// Проверяет доступность конвертера
func (c complexConverterProcessor) IsAvailable(object interface{}) bool {
	objectType := reflect.TypeOf(object).Kind()

	return objectType == reflect.Complex64 || objectType == reflect.Complex128
}

// Обработка значения конвертером
func (c complexConverterProcessor) Process(object interface{}, pointers []uintptr) processResult {
	return processResult{fmt.Sprintf("%v", object), pointers}
}

// Фабрика процессора
func newComplexConverterProcessor() converterProcessorInterface {
	return &complexConverterProcessor{}
}
