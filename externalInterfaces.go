package structure_dumper

// Конвертер сложных объектов в простую коллекцию
type StructureConverterInterface interface {
	// Конвертирует переданный объект данных в простую коллекцию значений или простое значение
	Convert(interface{}) interface{}
}
