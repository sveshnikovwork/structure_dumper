package structure_dumper

import (
	"fmt"
	"time"
)

// Конвертер дат
type timeConverterProcessor struct{}

// Сеттер основного конвертера
func (s *timeConverterProcessor) SetConverter(structureConverterInterface) {}

// Проверяет доступность конвертера
func (s timeConverterProcessor) IsAvailable(object interface{}) bool {
	if _, ok := object.(time.Time); ok {
		return true
	}

	return false
}

// Обработка значения конвертером
func (s timeConverterProcessor) Process(object interface{}, pointers []uintptr) processResult {
	date := object.(time.Time)

	return processResult{fmt.Sprintf(`time(%v)`, date.Format(time.RFC3339)), pointers}
}

// Фабрика процессора
func newTimeConverterProcessor() converterProcessorInterface {
	return &timeConverterProcessor{}
}
