package structure_dumper

import (
	"reflect"
	"testing"
)

// Тестовая именованная структура
type namedTest struct {
	test int
}

// Тестирование метода доступности процессора
func Test_structConverterProcessor_IsAvailable(t *testing.T) {
	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "Тестирование передачи функции",
			fields: fields{&converterMock{}},
			args: args{
				func() {},
			},
			want: false,
		},
		{
			name:   "Тестирование передачи структуры",
			fields: fields{&converterMock{}},
			args: args{
				struct{}{},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := structConverterProcessor{
				converter: tt.fields.converter,
			}
			if got := s.IsAvailable(tt.args.object); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование метода обработки процессора
func Test_structConverterProcessor_Process(t *testing.T) {
	type fields struct {
		converter structureConverterInterface
	}
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   processResult
	}{
		{
			name:   "Тестирование передачи структуры c логическим значением с не пустым массивом указателей",
			fields: fields{&converterMock{}},
			args:   args{struct{ test bool }{false}, []uintptr{1}},
			want: processResult{map[string]interface{}{
				"struct":  "anonymous",
				"package": "-",
				"fields": map[string]interface{}{
					"test": false,
				},
			}, []uintptr{1}},
		},
		{
			name:   "Тестирование передачи структуры c подструктурой с не пустым массивом указателей",
			fields: fields{&converterMock{}},
			args:   args{struct{ test struct{} }{struct{}{}}, []uintptr{1}},
			want: processResult{map[string]interface{}{
				"struct":  "anonymous",
				"package": "-",
				"fields": map[string]interface{}{
					"test": struct{}{},
				},
			}, []uintptr{1}},
		},
		{
			name:   "Тестирование передачи структуры c строковым значением с не пустым массивом указателей",
			fields: fields{&converterMock{}},
			args:   args{struct{ test string }{`false`}, []uintptr{1}},
			want: processResult{map[string]interface{}{
				"struct":  "anonymous",
				"package": "-",
				"fields": map[string]interface{}{
					"test": `false`,
				},
			}, []uintptr{1}},
		},
		{
			name:   "Тестирование передачи структуры c массивом с не пустым массивом указателей",
			fields: fields{&converterMock{}},
			args:   args{struct{ test []string }{[]string{`false`}}, []uintptr{1}},
			want: processResult{map[string]interface{}{
				"struct":  "anonymous",
				"package": "-",
				"fields": map[string]interface{}{
					"test": []string{`false`},
				},
			}, []uintptr{1}},
		},
		{
			name:   "Тестирование передачи структуры c числовым значением с не пустым массивом указателей",
			fields: fields{&converterMock{}},
			args:   args{struct{ test int }{22}, []uintptr{1}},
			want: processResult{map[string]interface{}{
				"struct":  "anonymous",
				"package": "-",
				"fields": map[string]interface{}{
					"test": 22,
				},
			}, []uintptr{1}},
		},
		{
			name:   "Тестирование передачи именованной структуры c числовым значением с не пустым массивом указателей",
			fields: fields{&converterMock{}},
			args:   args{namedTest{22}, []uintptr{1}},
			want: processResult{map[string]interface{}{
				"struct":  "namedTest",
				"package": reflect.TypeOf(namedTest{22}).PkgPath(),
				"fields": map[string]interface{}{
					"test": 22,
				},
			}, []uintptr{1}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := structConverterProcessor{
				converter: tt.fields.converter,
			}
			if got := s.Process(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Process() = %v, want %v", got, tt.want)
			}
		})
	}
}
