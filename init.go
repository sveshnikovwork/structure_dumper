package structure_dumper

import "reflect"

var simpleTypes []reflect.Kind
var unprocessableTypes []reflect.Kind

var structureConverterSingleton structureConverterInterface

func init() {
	simpleTypes = []reflect.Kind{
		reflect.String,
		reflect.Bool,
		reflect.Float32,
		reflect.Float64,
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
	}

	unprocessableTypes = []reflect.Kind{
		reflect.Func,
		reflect.Chan,
		reflect.Interface,
		reflect.UnsafePointer,
		reflect.Invalid,
	}

	structureConverterSingleton = newStructureConverter()
}
