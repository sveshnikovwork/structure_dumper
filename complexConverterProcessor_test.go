package structure_dumper

import (
	"reflect"
	"testing"
)

// Тестирование метода доступности процессора
func Test_complexConverterProcessor_IsAvailable(t *testing.T) {
	type args struct {
		object interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование передачи функции",
			args: args{
				func() {},
			},
			want: false,
		},
		{
			name: "Тестирование передачи комплексного числа",
			args: args{
				complex(1, 2),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := complexConverterProcessor{}
			if got := c.IsAvailable(tt.args.object); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование метода обработки процессора
func Test_complexConverterProcessor_Process(t *testing.T) {
	type args struct {
		object   interface{}
		pointers []uintptr
	}
	tests := []struct {
		name string
		args args
		want processResult
	}{
		{
			name: "Тестирование передачи комплексного числа 1+2i",
			args: args{
				complex(1, 2),
				[]uintptr{},
			},
			want: processResult{"(1+2i)", []uintptr{}},
		},
		{
			name: "Тестирование передачи комплексного числа -1+2i",
			args: args{
				complex(-1, 2),
				[]uintptr{},
			},
			want: processResult{"(-1+2i)", []uintptr{}},
		},
		{
			name: "Тестирование передачи комплексного числа 1-2i",
			args: args{
				complex(1, -2),
				[]uintptr{},
			},
			want: processResult{"(1-2i)", []uintptr{}},
		},
		{
			name: "Тестирование передачи комплексного числа -1-2i",
			args: args{
				complex(-1, -2),
				[]uintptr{},
			},
			want: processResult{"(-1-2i)", []uintptr{}},
		},
		{
			name: "Тестирование передачи не пустого набора указателей",
			args: args{
				complex(-1, -2),
				[]uintptr{1},
			},
			want: processResult{"(-1-2i)", []uintptr{1}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := complexConverterProcessor{}
			if got := c.Process(tt.args.object, tt.args.pointers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Process() = %v, want %v", got, tt.want)
			}
		})
	}
}
